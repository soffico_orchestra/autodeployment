## Autodeployment Description
**Orchestra Runtime is able to deploy scenarios, update scenario credentials and update landscape without Monitor interaction.**
```
Configuration
```
<p>
<details>
<summary>Enable Autodeployment</summary>
These details <em>will</em> remain <strong>hidden</strong> until expanded.
<h2>Orchestra Monitor->Settings-> Environment Settings -> runtime</h2>
<dl>
  <dt>Autodeployment directory</dt>
  <dd>
 Therefore change environment setting Runtime.autodeployment.dir to an empty directory, e.g. C:\tmp\ORCHESTRA\autodeployment.
 <h1>save new configuration and restart orchestra server</h1>
  </dd>
</dl>
![autodeployment](/uploads/234e87701dcfe076bb27c06f66089c6f/autodeployment.png)
</details>
</p>

```
set more Loginfo (optional)
```
<p>
<details>
<summary>Autodeployment Loginfo</summary>
These details <em>will</em> remain <strong>hidden</strong> until expanded.
<br>
open "~/orchestra/WEB-INF/classes/logging.properties" and add "emds.epi.autodeployment.level = INFO"
<br>
![AutodeploymentLog](/uploads/42325f6d1fe292423d50d593abdff8e5/AutodeploymentLog.PNG)

</details>
</p>

```
Operation
```
<p>
<details>
<summary>how is worked</summary>
<h1>put scenarios in the directory</h1>
<summary><h2>before deployment</h2></summary>
![beforeDeploy](/uploads/41899c75b89abdff6223ffb9068cf340/beforeDeploy.PNG)
<br>
<summary><h2>after deployment</h2></summary>
![afterDeploy](/uploads/e833053108973f2f1ec9c5bd98127cf0/afterDeploy.PNG)
</details>
</p>
